package hash

import "fmt"

func PasswordMd5(password string, salt string) string {
	saltedPassword := fmt.Sprintf("%s%s", password, salt)
	hashedPassword := Md5(saltedPassword)
	return hashedPassword
}

func PasswordSha1(password string, salt string) string {
	saltedPassword := fmt.Sprintf("%s%s", password, salt)
	hashedPassword := Sha1(saltedPassword)
	return hashedPassword
}

func PasswordSha256(password string, salt string) string {
	saltedPassword := fmt.Sprintf("%s%s", password, salt)
	hashedPassword := Sha256(saltedPassword)
	return hashedPassword
}

func PasswordSha512(password string, salt string) string {
	saltedPassword := fmt.Sprintf("%s%s", password, salt)
	hashedPassword := Sha256(saltedPassword)
	return hashedPassword
}

func PasswordBcrypt(password string, pepper string) string {
	saltedPassword := fmt.Sprintf("%s%s", password, pepper)
	hashedPassword := Bcrypt(saltedPassword)
	return hashedPassword
}
