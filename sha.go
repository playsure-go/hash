package hash

import (
	hashSha1 "crypto/sha1"
	hashSha256 "crypto/sha256"
	hashSha512 "crypto/sha512"
	"encoding/hex"
)

func Sha1(message string) string {
	sha1 := hashSha1.New()
	sha1.Write([]byte(message))
	sha1Data := sha1.Sum(nil)
	return hex.EncodeToString(sha1Data)
}

func Sha256(message string) string {
	sha256 := hashSha256.New()
	sha256.Write([]byte(message))
	sha256Data := sha256.Sum(nil)
	return hex.EncodeToString(sha256Data)
}

func Sha512(message string) string {
	sha512 := hashSha512.New()
	sha512.Write([]byte(message))
	sha256Data := sha512.Sum(nil)
	return hex.EncodeToString(sha256Data)
}
