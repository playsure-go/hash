package hash

import "encoding/hex"

func HmacMd5HexString(message string, key string) string {
	hmacMd5Data := HmacMd5([]byte(message), []byte(key))
	return hex.EncodeToString(hmacMd5Data)
}

func HmacSha1HexString(message string, key string) string {
	hmacSha1Data := HmacSha1([]byte(message), []byte(key))
	return hex.EncodeToString(hmacSha1Data)
}

func HmacSha256HexString(message string, key string) string {
	hmacSha256Data := HmacSha256([]byte(message), []byte(key))
	return hex.EncodeToString(hmacSha256Data)
}

func HmacSha512HexString(message string, key string) string {
	hmacSha512Data := HmacSha512([]byte(message), []byte(key))
	return hex.EncodeToString(hmacSha512Data)
}
