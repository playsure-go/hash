package hash

import (
	hashMd5 "crypto/md5"
	"encoding/hex"
)

func Md5(message string) string {
	md5 := hashMd5.New()
	md5.Write([]byte(message))
	md5Data := md5.Sum(nil)
	return hex.EncodeToString(md5Data)
}
