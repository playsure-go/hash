package hash

import "encoding/base64"

func HmacMd5Base64String(message string, key string) string {
	hmacMd5Data := HmacMd5([]byte(message), []byte(key))
	return base64.StdEncoding.EncodeToString(hmacMd5Data)
}

func HmacSha1Base64String(message string, key string) string {
	hmacSha1Data := HmacSha1([]byte(message), []byte(key))
	return base64.StdEncoding.EncodeToString(hmacSha1Data)
}

func HmacSha256Base64String(message string, key string) string {
	hmacSha256Data := HmacSha256([]byte(message), []byte(key))
	return base64.StdEncoding.EncodeToString(hmacSha256Data)
}

func HmacSha512Base64String(message string, key string) string {
	hmacSha512Data := HmacSha512([]byte(message), []byte(key))
	return base64.StdEncoding.EncodeToString(hmacSha512Data)
}
