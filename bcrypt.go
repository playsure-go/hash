package hash

import "golang.org/x/crypto/bcrypt"

func Bcrypt(message string) string {
	return BcryptWithCost(message, bcrypt.DefaultCost)
}

func BcryptWithCost(message string, cost int) string {
	if cost < bcrypt.MinCost || cost > bcrypt.MaxCost { // Invalid cost.
		return ""
	}
	bcryptData, err := bcrypt.GenerateFromPassword([]byte(message), cost)
	if err != nil { // Failure.
		return ""
	}
	return string(bcryptData)
}

func BcryptCheck(hashedMessage string, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedMessage), []byte(password))
	if err == nil { // Success.
		return true
	}
	return false
}

func BcryptGetCost(hashedMessage string) int {
	cost, err := bcrypt.Cost([]byte(hashedMessage))
	if err != nil { // Failure.
		return 0
	}
	return cost
}
