package hash

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
)

func HmacMd5(data []byte, key []byte) []byte {
	hmacMd5 := hmac.New(md5.New, key)
	hmacMd5.Write(data)
	hmacMd5Data := hmacMd5.Sum(nil)
	return hmacMd5Data
}

func HmacSha1(data []byte, key []byte) []byte {
	hmacSha1 := hmac.New(sha1.New, key)
	hmacSha1.Write(data)
	hmacSha1Data := hmacSha1.Sum(nil)
	return hmacSha1Data
}

func HmacSha256(data []byte, key []byte) []byte {
	hmacSha256 := hmac.New(sha256.New, key)
	hmacSha256.Write(data)
	hmacSha256Data := hmacSha256.Sum(nil)
	return hmacSha256Data
}

func HmacSha512(data []byte, key []byte) []byte {
	hmacSha512 := hmac.New(sha512.New, key)
	hmacSha512.Write(data)
	hmacSha512Data := hmacSha512.Sum(nil)
	return hmacSha512Data
}
